<?php

namespace App\Notification;

use App\Entity\Comment;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\Recipient;

class CommentReviewNotification extends Notification implements EmailNotificationInterface
{
    private Comment $comment;

    private string  $reviewUrl;

    public function __construct(Comment $comment, string $reviewUrl)
    {
        $this->comment = $comment;
        $this->reviewUrl = $reviewUrl;

        parent::__construct('New comment posted');
    }

    public function asEmailMessage(Recipient $recipient, ?string $transport = null): ?EmailMessage
    {
        $message = EmailMessage::fromNotification($this, $recipient, $transport);
        $message->getMessage()
            ->htmlTemplate('emails/comment_notification.html.twig')
            ->context(['comment' => $this->comment]);

        return $message;
    }

    public function asChatMessage(Recipient $recipient, ?string $transport = null): ?ChatMessage
    {
        if ($transport !== 'telegram') {
            return null;
        }

        $message = ChatMessage::fromNotification($this, $recipient, $transport);

        return $message;
    }

    public function getChannels(Recipient $recipient): array
    {
        
        if (preg_match('{\b(great|awesome)\b}i', $this->comment->getText())) {
            return ['email', 'chat/slack'];
        }

        $this->importance(Notification::IMPORTANCE_LOW);

        return ['email'];
    }
}
