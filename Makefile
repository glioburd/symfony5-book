SHELL := /bin/bash

tests:
	symfony console doctrine:fixtures:load -n
	symfony run bin/phpunit

start:
	docker-compose start
	symfony serve -d
	symfony run -d --watch=config,src,templates,vendor symfony console messenger:consume async
	symfony run -d yarn encore dev --watch
	#symfony server:start -d --passthru=index.html

stop:
	docker-compose stop
	symfony server:stop

.PHONY: tests start stop restart
